import { Component } from '@angular/core';
import { MainFacade } from './main/state/facade';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  onChangeText(event: any) {
    this.mainFacade.changeTitle(event.target.value as string);
  }

  constructor(
    private mainFacade: MainFacade
  ) { }
}
