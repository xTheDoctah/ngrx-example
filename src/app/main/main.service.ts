import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient } from '@angular/common/http'
import { delay } from "rxjs/operators";


export interface Todo {
    userId: number,
    id: string,
    body: string,
    title: string
}

@Injectable()
export class MainService {
    public getTodos(): Observable<Todo[]> {
        return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/posts').pipe(delay(10000));
    }

    constructor(
        private http: HttpClient
    ) { }
}