import { createReducer, on } from '@ngrx/store';
import { Todo } from '../main.service';
import * as MainActions from './actions';

export interface MainState {
    title: string,
    todos: Todo[],
    isLoadingTodos: boolean
};

export const initialState: MainState = {
    title: 'Hello!',
    todos: [],
    isLoadingTodos: false
};

export const mainReducer = createReducer(
    initialState,
    on(MainActions.changeTitleAction, (state, action) => {
        return {
            ...state,
            title: action.title
        };
    }),
    on(MainActions.loadTodos, (state, action) => {
        return {
            ...state,
            isLoadingTodos: true
        }
    }),
    on(MainActions.todosLoaded, (state, action) => {
        return {
            ...state,
            todos: action.todos,
            isLoadingTodos: false
        }
    })
);