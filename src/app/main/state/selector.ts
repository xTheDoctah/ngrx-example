import { createFeatureSelector, createSelector } from "@ngrx/store";
import { MainState } from './reducer';

export const mainState = createFeatureSelector<MainState>('main');

export const selectTitle = createSelector(
    mainState,
    (state: MainState) => state.title
);

export const selectTodos = createSelector(
    mainState,
    (state: MainState) => state.todos
);

export const selectIsLoadingTodos = createSelector(
    mainState,
    (state: MainState) => state.isLoadingTodos
);