import { Injectable } from "@angular/core";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { MainState } from "./reducer";
import * as MainSelectors from './selector';
import * as MainActions from './actions';
import { Todo } from "../main.service";

@Injectable()
export class MainFacade {
    // Selectors
    public getTitle$: Observable<string> = this.store.pipe(
        select(MainSelectors.selectTitle)
    );

    public getTodos$: Observable<Todo[]> = this.store.pipe(
        select(MainSelectors.selectTodos)
    );

    public isLoadingTodos$: Observable<boolean> = this.store.pipe(
        select(MainSelectors.selectIsLoadingTodos)
    );

    // Actions
    public changeTitle(title: string): void {
        this.store.dispatch(
            MainActions.changeTitleAction({ title })
        );
    }

    public loadTodos(): void {
        this.store.dispatch(
            MainActions.loadTodos()
        );
    }

    constructor(
        private store: Store<MainState>
    ) { }
}