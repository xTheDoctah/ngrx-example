import { createAction, props } from "@ngrx/store";
import { Todo } from "../main.service";


export const changeTitleAction = createAction(
    '[Homepage] Change Title Action',
    props<{ title: string }>()
);

export const loadTodos = createAction(
    '[Homepage] Load Todos'
);


export const todosLoaded = createAction(
    '[Homepage] Todos Loaded',
    props<{ todos: Todo[] }>()
);