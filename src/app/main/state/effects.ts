import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap } from "rxjs/operators";
import { MainService } from "../main.service";
import * as MainActions from './actions';


@Injectable()
export class MainEffects {

    getTodos$ = createEffect(() => this.actions$.pipe(
        ofType(MainActions.loadTodos),
        mergeMap(action => this.mainService.getTodos().pipe(
            map(todos => MainActions.todosLoaded({ todos }))
        )),
    ));

    constructor(
        private actions$: Actions,
        private mainService: MainService
    ) { }
}