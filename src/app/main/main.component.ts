import { Component, OnDestroy, OnInit } from "@angular/core";
import { Observable, ReplaySubject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";
import { MainService, Todo } from "./main.service";
import { MainFacade } from "./state/facade";


@Component({
    selector: 'main-component',
    templateUrl: 'main.component.html',
    styleUrls: ['main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
    private destroy$ = new ReplaySubject<boolean>();

    public title$: Observable<string> = this.mainFacade.getTitle$.pipe(
        takeUntil(this.destroy$)
    );

    public todos$: Observable<Todo[]> = this.mainFacade.getTodos$.pipe(
        takeUntil(this.destroy$)
    );

    public isLoadingTodos$: Observable<boolean> = this.mainFacade.isLoadingTodos$.pipe(
        takeUntil(this.destroy$)
    );

    public ngOnInit(): void {
        this.mainFacade.loadTodos();
    }

    constructor(
        private mainFacade: MainFacade
    ) {
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

}