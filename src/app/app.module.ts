import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MainComponent } from './main/main.component';
import { mainReducer } from './main/state/reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { MainFacade } from './main/state/facade';
import { MainService } from './main/main.service';
import { HttpClientModule } from '@angular/common/http';
import { MainEffects } from './main/state/effects';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StoreModule.forRoot({
      main: mainReducer
    }, {}),
    EffectsModule.forRoot([MainEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [
    MainService,
    MainFacade
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

